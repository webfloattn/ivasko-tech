<?php
$response = ['error' => true];
$secret = "6LeCEAYaAAAAABw17vhE5nAu4riDKC7aPEa5qwZS";
try {
  if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone']) || empty($_POST['message']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $response['message'] = 'Por favor preencha todos os campos necessários.';
  }
  $name = strip_tags(htmlspecialchars($_POST['name']));
  $email_address = strip_tags(htmlspecialchars($_POST['email']));
  $phone = strip_tags(htmlspecialchars($_POST['phone']));
  $message = strip_tags(htmlspecialchars($_POST['message']));

  $to = 'contato@ivaskotech.com.br';
  $email_remetente = 'contato@ivaskotech.com.br';
  $email_subject = "Contato via Site Ivasko Tech";

  $email_body = "
    <style type='text/css'>
    body {
        margin: 0px;
        font-family: 'Brother Regular';
        color: black;
    }

    .header {
        width: 100%;
        padding: 30px 0;
        display: flex;
        justify-content: center;
        background-image: linear-gradient(160deg, #dd4c23 0, #f27c1e 57%);
    }

    .container {
        width: 80%;
        margin-right: auto;
        margin-left: auto;
        margin-bottom: 50px;
    }

    h1 {
        text-align: center;
        font-size: 30px;
        color: #dd4c23;
    }

    .header-description p {
        font-size: 20px;
        color: black;
    }

    @font-face {
        font-family: 'Brother Regular';
        font-style: normal;
        font-weight: 500;
        src: url('./fonts/Brother-1816-Regular.woff') format('woff');
    }

    hr {
        border-top: 1px solid #e7e9eb;
      }
</style>
<html>

<div class='container'>
<h1> CONTATO ATRAVÉS DO SITE IVASKO TECH </h1>
<hr>
    <p><strong>Nome: </strong>$name</p>
    <p><strong>E-mail: </strong>$email_address</p>
    <p><strong>Telefone: </strong>$phone</p>
    <p><strong>Mensagem: </strong>$message</p>

</div>

</html>
    ";

  // $headers  = 'MIME-Version: 1.0' . "\r\n";
  // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  // $headers .= "From: " . $email_address . "\n";
  // $headers .= "Reply-To: $email_address";

  $headers = implode("\n", array("From: $email_remetente", "Reply-To: $to", "Return-Path: $email_remetente", "MIME-Version: 1.0", "X-Priority: 3", "Content-Type: text/html; charset=UTF-8"));


  mail($to, $email_subject, $email_body, $headers);

  $response['error'] = false;
} catch (Exception $e) {
  $response['error'] = true;
  $response['message'] = 'Não foi possível enviar a mensage, por favor tente novamente.';
}

echo json_encode($response);
return;
